Risketos Bàsics

 - [X] Capacitat d’elecció, per exemple diferents torretes.
 - [X] El HUD es crea i s’emplena per BP.
 - [X] Ús d’esdeveniments i dispatchers, tot ha d’estar desacoplat.
 - [X] Enemics o objectius a on atacar i en fer-ho que morin.
 - [X] Ús d’un actor component o Interfície.

Risketos Opcionals

 - [ ] Fer una pool d’objectes.
 - [ ] Augment progressiu de dificultat.
 - [ ] Mapa de joc generat de manera procedimental.
 - [ ] Menú inicial amb diversos nivells.
 - [X] “Waves” personalitzables a on posar quantitat d’enemics i tipus d’enemics a aparèixer.











 





